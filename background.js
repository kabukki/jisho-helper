/**
 * Search for a given string
 * @param {*} text text to search
 */
function search (text) {
	const base = 'https://www.jisho.org';
	return encodeURI(`${base}/search/`) + encodeURIComponent(text);
}

/**
 * Get whether a character is in the kanji unicode range
 * @param {*} character 
 */
function isKanji (character) {
	const range = { min: 0x4e00, max: 0x9faf };
	const charCode = character.charCodeAt(0);
	return charCode >= range.min && charCode <= range.max;
}

/**
 * Callback on click event with selection context
 * @param {*} info 
 * @param {*} tab 
 */
function onClick (info, tab) {
	if (info.selectionText) {
		const text = info.selectionText.trim();
		const url = (text.length === 1 && isKanji(text)) ? search(`${text} #kanji`) : search(text);
		chrome.tabs.create({ url });
	}
}

/**
 * Add an item to the context menu when selecting text
 */
chrome.contextMenus.create({
	title: 'Look up "%s" on Jisho',
	contexts: ['selection'],
	onclick: onClick
}, _ => {
	if (chrome.runtime.lastError) {
		console.log(chrome.runtime.lastError);
	}
});
